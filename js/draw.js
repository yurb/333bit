DEBUG = false;

// A bit of head room at the top
view.translate(new Point(0, 50));

wind = new Point(0, 0);
threshold = new Point(0.05, 0.05);

var bg = new Shape.Rectangle(
    new Point(0, 0),
    view.size);
bg.fillColor = 'black';

function initPoints (n) {
    var points = new Array(n);
    for (var i = 0; i < n; i++) {
        var p = new Shape.Circle(
            new Point(
                Math.random() * view.viewSize.width,
                Math.random() * view.viewSize.height),
            2);
        p.fillColor = 'white';
        p.velocity = new Point(0, 0);
        points[i] = p;
    }
    return points;
}

function initCross () {
    return new Path({
        segments: [
            [225 + 100, 50],
            [275 + 100, 50],
            [275 + 100, 225],
            [475 + 100, 225],
            [475 + 100, 275],
            [275 + 100, 275],
            [275 + 100, 800],
            [225 + 100, 800],
            [225 + 100, 275],
            [25 + 100, 275],
            [25 + 100, 225],
            [225 + 100, 225],
        ],
        //strokeColor: '#ddd',
        closed: true,
    });
}

function brownian (scale) {
    scale = scale ? scale : 1;
    return ((Math.random() - 0.5) * 0.5 * scale);
};

var points = initPoints(333);
var cross = initCross();

if (DEBUG) {
    var center = new Shape.Circle({
        center: view.center,
        radius: 5,
        fillColor: 'red'
    });

    var indicator = new Path({
        segments: [
            view.center,
            view.center
        ],
        strokeColor: 'red',
        strokeWidth: 4,
    });
}

function onFrame (event) {
    points.forEach(function (p, i) {
        // Test if p is within cross
        if (! cross.contains(p.position)) {
            // Find closest point
            var target = cross.getNearestPoint(p.position);
            var vector = target - p.position;
            p.position += vector / 30;
        }
        p.velocity.x += brownian(3);
        p.velocity.y += brownian(3);

        // Calculate "wind" direction
        if (wind.abs() > threshold && wind.abs() > p.velocity.abs()) {
            p.velocity += wind;
        }
        p.position += p.velocity / 3;
        p.velocity = p.velocity * 0.99;

        if (DEBUG) {
            indicator.segments = [
                view.center,
                view.center + (wind * 4),
            ];
        }
    });
}
