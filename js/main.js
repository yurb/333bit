'use strict';

function initCanvas (v) {
    var canvas = document.getElementById("cam");
    var ctx = canvas.getContext('2d');

    var flow = new oflow.CanvasFlow(canvas);

    flow.onCalculated(function (direction) {
        if (!isNaN(direction.u))
            wind.x = direction.u * 2 * -1;
        if (!isNaN(direction.v))
            wind.y = direction.v * 2;
    });

    flow.startCapture();

    cameraFrame(v, ctx);
};

function cameraFrame (video, ctx) {
    ctx.drawImage(video, 0, 0, 64, 48);
    requestAnimationFrame(function () { cameraFrame(video, ctx); });
}

navigator.mediaDevices.getUserMedia({video: true, audio: false})
    .then(function(stream) {
        var v = document.getElementById("cam-in");
        v.srcObject = stream;
        v.onloadedmetadata = function (e) {
            v.play();
            initCanvas(v);
        }
    });
